package bearmaps.proj2ab;
import java.util.List;

public class KDTree implements PointSet {

    private Node root;

    private class Node {
        Point point;
        double x;
        double y;
        Node left, right;
        boolean orientation;

        Node(Point p, boolean orientation) {
            point = p;
            x = p.getX();
            y = p.getY();
            this.orientation = orientation;
        }
    }

    public KDTree(List<Point> points) {
        for (Point p: points) {
            root = add(root, p, true);
        }
    }

    private Node add(Node n, Point p, boolean horizontal) {
        if (n == null) {
            return new Node(p, horizontal);
        }

        if (p.equals(n.point)) {
            return n;
        }

        if (comparePoints(n.point, p, n.orientation) < 0) {
            n.left = add(n.left, p, !horizontal);
        } else if (comparePoints(n.point, p, n.orientation) >= 0) {
            n.right = add(n.right, p, !horizontal);
        }
        return n;
    }

    public Point nearest(double x, double y) {
        Point goal = new Point(x, y);
        Node best = nearestHelper(root, goal, root, root.orientation);
        return best.point;
    }

    private Node nearestHelper(Node n, Point goal, Node best, boolean horizontal) {
        if (n == null) {
            return best;
        }

        if (Point.distance(goal, n.point) < Point.distance(goal, best.point)) {
            best = n;
        }

        Node good;
        Node bad;

        if (comparePoints(n.point, goal, n.orientation) < 0) {
            good = n.left;
            bad = n.right;
        } else {
            good = n.right;
            bad = n.left;
        }

        best = nearestHelper(good, goal, best, !horizontal);

        if (dummyPoint(n, goal) < Point.distance(best.point, goal)) {
            best = nearestHelper(bad, goal, best, !horizontal);
        }
        return best;
    }

    private int comparePoints(Point a, Point b, boolean horizontal) {
        if (horizontal) {
            return Double.compare(a.getX(), b.getX());
        } else {
            return Double.compare(a.getY(), b.getY());
        }
    }

    private double dummyPoint(Node n, Point goal) {
        if (n.orientation) {
            Point p1 = new Point(n.point.getX(), goal.getY());
            return Point.distance(p1, goal);
        } else {
            Point p2 = new Point(goal.getX(), n.point.getY());
            return Point.distance(p2, goal);
        }
    }

    private Node slowNearestHelper(Node n, Point goal, Node best) {
        if (n == null) {
            return best;
        }

        if (Point.distance(goal, n.point) < Point.distance(goal, best.point)) {
            best = n;
        }

        best = slowNearestHelper(n.left, goal, best);
        best = slowNearestHelper(n.right, goal, best);
        return best;
    }
}
