package bearmaps.proj2ab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;

// import static bearmaps.PrintHeapDemo.printFancyHeapDrawing;

/**
 * The ArrayHeapMinPQ Class
 * @author Sooyeon Oh
 * @source CS61B Lecture 21
 * */

public class ArrayHeapMinPQ<T> implements ExtrinsicMinPQ<T> {

    private ArrayList<Node> keys;
    private int size;
    private HashMap<T, Integer> map;

    public ArrayHeapMinPQ() {
        keys = new ArrayList<Node>();
        size = 0;
        map = new HashMap<T, Integer>();
    }

    @Override
    public void add(T item, double priority) {
        if (this.contains(item)) {
            throw new IllegalArgumentException("Cannot add an item that already exists in queue.");
        }
        Node n = new Node(item, priority);
        keys.add(n);
        map.put(item, size);
        swim(size, n);
        size++;
    }

    private void swim(int nI, Node n) {
        int pI = parent(nI);
        Node p = keys.get(pI);
        if (n.priority < p.priority) {
            swap(nI, pI);
            swim(pI, n);
        }
    }

    private void swap(int xIndex, int yIndex) {
        Node x = keys.get(xIndex);
        Node y = keys.get(yIndex);
        keys.set(xIndex, y);
        keys.set(yIndex, x);
        map.put(x.item, yIndex);
        map.put(y.item, xIndex);
    }

    @Override
    public boolean contains(T item) {
        return map.containsKey(item);
    }

    @Override
    public T getSmallest() {
        if (size == 0) {
            throw new NoSuchElementException("Smallest item does not exist.");
        }
        return keys.get(0).item;
    }

    @Override
    public T removeSmallest() {
        if (size == 0) {
            throw new NoSuchElementException("Smallest item does not exist.");
        }
        T value = replaceRoot();
        if (size > 1) {
            sink(0);
        }
        size--;
        return value;
    }

    @Override
    public void changePriority(T item, double priority) {
        if (!this.contains(item)) {
            throw new NoSuchElementException("Item does not exist.");
        }
        int index = map.get(item);
        Node n = keys.get(index);
        n.priority = priority;
        if (n.priority < getParent(index).priority) {
            swim(index, n);
        } else {
            sink(index);
        }
    }

    private void sink(int nI) {
        // index is out of bounds
        if (nI == keys.size()) {
            return;
        } else if (left(nI) >= keys.size() && right(nI) >= keys.size()) {
            return;
        }

        Node n = keys.get(nI);
        // case for 2 children
        if (left(nI) < keys.size() && right(nI) < keys.size()
                && n.priority > Math.min(getLeftChild(nI).priority, getRightChild(nI).priority)) {
            if (getLeftChild(nI).priority < getRightChild(nI).priority) {
                swap(nI, left(nI));
                sink(left(nI));
            } else {
                swap(nI, right(nI));
                sink(right(nI));
            }
        } else if (left(nI) < keys.size() && n.priority > getLeftChild(nI).priority) {
            swap(nI, left(nI));
            sink(left(nI));
        } else if (right(nI) < keys.size() && n.priority > getRightChild(nI).priority) {
            swap(nI, right(nI));
            sink(right(nI));
        }
    }

    private T replaceRoot() {
        T value = keys.get(0).item;
        int lastIndex = keys.size() - 1;
        swap(0, lastIndex);
        keys.remove(lastIndex);
        map.remove(value);
        return value;
    }

    @Override
    public int size() {
        return size;
    }

    private static void main(String[] args) {
        ArrayHeapMinPQ<Object> heap = new ArrayHeapMinPQ();
        heap.add('a', 1);
        heap.add('b', 2);
        heap.add('c', 3);
        heap.add('d', 0);
        heap.removeSmallest();
        heap.add('e', 0);
        heap.changePriority('e', 3.5);

        Object[] lst = new Object[heap.size() + 1];
        for (int i = 0; i < heap.size(); i++) {
            lst[i + 1] = heap.keys.get(i).item;
        }
        // printFancyHeapDrawing(lst);
    }

    private int parent(int k) {
        return k / 2;
    }

    private int left(int k) {
        return 2 * k;
    }

    private int right(int k) {
        return 2 * k + 1;
    }

    private Node getParent(int k) {
        return keys.get(parent(k));
    }

    private Node getLeftChild(int k) {
        return keys.get(left(k));
    }

    private Node getRightChild(int k) {
        return keys.get(right(k));
    }

    private class Node {
        private double priority;
        private T item;

        Node(T t, double p) {
            item = t;
            priority = p;
        }
    }
}
