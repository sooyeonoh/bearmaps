package bearmaps.proj2c;

import bearmaps.proj2ab.ArrayHeapMinPQ;
import edu.princeton.cs.algs4.Stopwatch;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class AStarSolver<Vertex> implements ShortestPathsSolver<Vertex> {
    private ArrayHeapMinPQ<Vertex> pq;
    private HashMap<Vertex, Double> distTo;
    private HashMap<Vertex, Vertex> edgeTo;
    private LinkedList<Vertex> solution;
    private SolverOutcome outcome;
    private int numStatesExplored;
    private double solutionWeight;
    private double timeElapsed;

    public AStarSolver(AStarGraph<Vertex> input, Vertex start, Vertex end, double timeout) {
        pq = new ArrayHeapMinPQ<>();
        distTo = new HashMap<>();
        edgeTo = new HashMap<>();
        solution = new LinkedList<>();
        numStatesExplored = 0;
        solutionWeight = 0;
        timeElapsed = 0;

        distTo.put(start, 0.0);
        pq.add(start, input.estimatedDistanceToGoal(start, end));

        Stopwatch sw = new Stopwatch();
        while (pq.size() > 0) {
            if (pq.getSmallest().equals(end)) {
                outcome = SolverOutcome.SOLVED;
                timeElapsed = sw.elapsedTime();

                Vertex v = end;
                solution.addFirst(end);
                while (!v.equals(start)) {
                    v = edgeTo.get(v);
                    solution.addFirst(v);
                }
                solutionWeight = distTo.get(end);
                return;
            }
            if (sw.elapsedTime() > timeout) {
                outcome = SolverOutcome.TIMEOUT;
                timeElapsed = sw.elapsedTime();
                return;
            }
            Vertex p = pq.removeSmallest();
            for (WeightedEdge<Vertex> e : input.neighbors(p)) {
                relax(e, end, input);
            }
            numStatesExplored += 1;
        }
        outcome = SolverOutcome.UNSOLVABLE;
        timeElapsed = sw.elapsedTime();
    }

    private void relax(WeightedEdge<Vertex> edge, Vertex end, AStarGraph<Vertex> input) {
        Vertex p = edge.from();
        Vertex q = edge.to();
        double w = edge.weight();
        double distToP = distTo.get(p);

        if (!distTo.containsKey(q) || distToP + w < distTo.get(q)) {
            distTo.put(q, distToP + w);
            edgeTo.put(q, p);
            if (pq.contains(q)) {
                pq.changePriority(q, distTo.get(q) + input.estimatedDistanceToGoal(q, end));
            } else {
                pq.add(q, distTo.get(q) + input.estimatedDistanceToGoal(q, end));
            }
        }
    }

    public SolverOutcome outcome() {
        return outcome;
    }

    public List<Vertex> solution() {
        return solution;
    }

    public double solutionWeight() {
        if (outcome == SolverOutcome.UNSOLVABLE || outcome == SolverOutcome.TIMEOUT) {
            return 0;
        } else {
            return solutionWeight;
        }
    }

    public int numStatesExplored() {
        return numStatesExplored;
    }

    public double explorationTime() {
        return timeElapsed;
    }
}
