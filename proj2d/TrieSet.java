package bearmaps.proj2d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * The TrieSet class using a HashMap.
 * @source CS61B Lecture 22
 * @author Sooyeon Oh
 * */

public class TrieSet {
    private Node root;
    private HashSet<String> keys;

    /** The Node class. */
    private class Node {
        private boolean isKey;
        private HashMap<Character, Node> children;

        public Node() {
            children = new HashMap<Character, Node>();
        }
    }

    /** TrieSet constructor. */
    public TrieSet() {
        root = new Node();
        keys = new HashSet<>();
    }

    /** Returns true if the trie contains the STR. */
    public boolean contains(String str) {
        return keys.contains(str);
    }

    /** Inserts a list of strings into the trie. */
    public void add(List<String> lst) {
        for (String str : lst) {
            add(str);
        }
    }

    /** Inserts string KEY into Trie. */
    public void add(String str) {
        if (str == null || str.length() == 0 || keys.contains(str)) {
            return;
        }
        Node curr = root;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!curr.children.containsKey(c)) {
                Node n = new Node();
                curr.children.put(c, n);
            }
            curr = curr.children.get(c);
        }
        curr.isKey = true;
        keys.add(str);
    }

    /** Returns a list of words that start with PREFIX. */
    public List<String> keysWithPrefix(String prefix) {
        List<String> matches = new ArrayList<String>();
        Node curr = root;

        for (int i = 0; i < prefix.length(); i++) {
            char c = prefix.charAt(i);
            if (curr.children.containsKey(c)) {
                curr = curr.children.get(c);
            } else {
                return matches;
            }
        }
        collectHelper(prefix, matches, curr);
        return matches;
    }

    private void collectHelper(String s, List<String> words, Node n) {
        if (n.isKey) {
            words.add(s);
        }
        for (char c : n.children.keySet()) {
            collectHelper(s + c, words, n.children.get(c));
        }
    }

    public static void main(String[] args) {
        TrieSet trie = new TrieSet();
        List<String> lst = List.of("sad", "sam", "same", "sap");
        trie.add(lst);
        // System.out.println(trie.keysWithPrefix("sa"));

        List<String> lst2 = List.of("a", "acafe", "acote", "adorapie", "abespizza", "abescafe", "accigallery");
        trie.add(lst2);
        System.out.println(trie.keysWithPrefix("a"));
    }
}
