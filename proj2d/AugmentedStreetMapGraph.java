package bearmaps.proj2d;

import bearmaps.proj2ab.KDTree;
import bearmaps.proj2ab.Point;
import bearmaps.proj2c.streetmap.StreetMapGraph;
import bearmaps.proj2c.streetmap.Node;

import java.lang.reflect.Array;
import java.util.*;

/**
 * An augmented graph that is more powerful that a standard StreetMapGraph.
 * Specifically, it supports the following additional operations:
 *
 *
 * @author Alan Yao, Josh Hug, Sooyeon Oh
 */
public class AugmentedStreetMapGraph extends StreetMapGraph {

    private List<Node> nodes;
    private HashMap<Point, Node> pointToNode;
    private List<Point> validPoints;
    private HashMap<String, HashSet<String>> cleanToFull;
    private HashMap<String, HashSet<Node>> cleanToNode;
    private TrieSet trie;
    private KDTree kdtree;

    public AugmentedStreetMapGraph(String dbPath) {
        super(dbPath);
        // You might find it helpful to uncomment the line below:
        nodes = this.getNodes();
        pointToNode = new HashMap<>();  // maps points to nodes
        cleanToFull = new HashMap<>();  // maps cleaned names to a set of full names
        cleanToNode = new HashMap<>();  // maps cleaned names to a set of nodes
        validPoints = new ArrayList<>();  // list of valid points to use for closest method
        trie = new TrieSet();

        for (Node n : nodes) {
            Point p = new Point(n.lon(), n.lat());
            if (!neighbors(n.id()).isEmpty()) {
                validPoints.add(p);
            }
            pointToNode.put(p, n);

            if (n.name() != null) {  // implementations for autocomplete functionality
                String cleaned = cleanString(n.name());
                trie.add(cleaned);

                if (cleanToFull.containsKey(cleaned)) {
                    cleanToFull.get(cleaned).add(n.name());
                } else {
                    HashSet<String> fullNames = new HashSet<>();
                    fullNames.add(n.name());
                    cleanToFull.put(cleaned, fullNames);
                }

                if (cleanToNode.containsKey(cleaned)) {
                    cleanToNode.get(cleaned).add(n);
                } else {
                    HashSet<Node> locations = new HashSet<>();
                    locations.add(n);
                    cleanToNode.put(cleaned, locations);
                }
            }
        }
        kdtree = new KDTree(validPoints);
    }

    /**
     * For Project Part II
     * Returns the vertex closest to the given longitude and latitude.
     * @param lon The target longitude.
     * @param lat The target latitude.
     * @return The id of the node in the graph closest to the target.
     */
    public long closest(double lon, double lat) {
        Point best = kdtree.nearest(lon, lat);
        return pointToNode.get(best).id();
    }

    /**
     * For Project Part III (gold points)
     * In linear time, collect all the names of OSM locations that prefix-match the query string.
     * @param prefix Prefix string to be searched for. Could be any case, with our without
     *               punctuation.
     * @return A <code>List</code> of the full names of locations whose cleaned name matches the
     * cleaned <code>prefix</code>.
     */
    public List<String> getLocationsByPrefix(String prefix) {
        List<String> words = trie.keysWithPrefix(cleanString(prefix));  // use keysWithPrefix to get list of matching strings
        List<String> output = new ArrayList<>();
        for (String str : words) {  // add every matching location that corresponds with the cleaned name
            for (String name : cleanToFull.get(str)) {
                    output.add(name);
            }
        }
        return output;
    }

    /**
     * For Project Part III (gold points)
     * Collect all locations that match a cleaned <code>locationName</code>, and return
     * information about each node that matches.
     * @param locationName A full name of a location searched for.
     * @return A list of locations whose cleaned name matches the
     * cleaned <code>locationName</code>, and each location is a map of parameters for the Json
     * response as specified: <br>
     * "lat" -> Number, The latitude of the node. <br>
     * "lon" -> Number, The longitude of the node. <br>
     * "name" -> String, The actual name of the node. <br>
     * "id" -> Number, The id of the node. <br>
     */
    public List<Map<String, Object>> getLocations(String locationName) {
        List<Map<String, Object>> output = new ArrayList<>();
        HashSet<Node> locations = cleanToNode.get(cleanString(locationName));
        for (Node n : locations) {
            Map<String, Object> map = new HashMap<>();
            map.put("lat", n.lat());
            map.put("lon", n.lon());
            map.put("name", n.name());
            map.put("id", n.id());
            output.add(map);
        }
        return output;
    }

    /**
     * Useful for Part III. Do not modify.
     * Helper to process strings into their "cleaned" form, ignoring punctuation and capitalization.
     * @param s Input string.
     * @return Cleaned string.
     */
    private static String cleanString(String s) {
        return s.replaceAll("[^a-zA-Z ]", "").toLowerCase();
    }

    public static void main(String [] args) {

    }
}
